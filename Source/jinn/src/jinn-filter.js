/*
 * @project: JINN
 * @version: 1.0
 * @license: MIT (not for evil)
 * @copyright: Yuriy Ivanov (Vtools) 2019-2020 [progr76@gmail.com]
 * Telegram:  https://t.me/progr76
*/

/**
 *
 * Responsible for the work safety policy
 * Contains algorithms for filtering unreliable nodes
 *
**/
'use strict';
global.JINN_MODULES.push({InitClass:InitClass});
function InitClass(Engine)
{
}
